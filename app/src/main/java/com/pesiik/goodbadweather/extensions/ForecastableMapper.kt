package com.pesiik.goodbadweather.extensions

import com.pesiik.entities.city.City
import com.pesiik.goodbadweather.room.entities.CityEntity

fun City.toEntity(): CityEntity =
    CityEntity(
        id = id,
        lon = lon,
        lat = lat,
        date = date,
        clouds = clouds,
        humidity = humidity,
        image = icon,
        maxTemperature = maxTemperature,
        minTemperature = minTemperature,
        name = name,
        pressure = pressure,
        temperature = temperature,
        windDegrees = windDegrees,
        windSpeed = windSpeed,
        country = country.toString()
    )

fun List<City>.toEntities(): List<CityEntity> =
    map {
        it.toEntity()
    }

fun CityEntity.toModel(): City =
    City(
        id = id,
        lon = lon,
        lat = lat,
        date = date,
        clouds = clouds,
        humidity = humidity,
        icon = image,
        maxTemperature = maxTemperature,
        minTemperature = minTemperature,
        name = name,
        pressure = pressure,
        temperature = temperature,
        windDegrees = windDegrees,
        windSpeed = windSpeed,
        country = country?.let { City.Country.valueOf(it) }
    )

fun List<CityEntity>.toCitiesModels() =
    map { it.toModel() }
