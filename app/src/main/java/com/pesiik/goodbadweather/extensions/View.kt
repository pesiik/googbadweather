package com.pesiik.goodbadweather.extensions

import android.view.View

fun View.makeVisible() {
    visibility = View.VISIBLE
}

fun View.makeInvisible() {
    visibility = View.INVISIBLE
}

fun View.makeGone() {
    visibility = View.GONE
}

fun View.makeActive() {
    isActivated = true
}

fun View.makeInactive() {
    isActivated = false
}

fun View.makeEnable() {
    isEnabled = true
}

fun View.makeDisable() {
    isEnabled = false
}

fun View.setOnOneClickListener(listener: () -> Unit) {
    setOnClickListener {
        listener()
        it.makeDisable()
        postDelayed({ it.makeEnable() }, 1000)
    }
}