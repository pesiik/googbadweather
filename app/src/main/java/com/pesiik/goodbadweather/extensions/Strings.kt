package com.pesiik.goodbadweather.extensions

import android.content.Context
import com.pesiik.entities.city.City
import com.pesiik.entities.common.Forecastable
import com.pesiik.goodbadweather.R

fun Forecastable.WindDirection.toString(context: Context): String =
    context.getString(
        when (this) {
            Forecastable.WindDirection.North -> R.string.north
            Forecastable.WindDirection.South -> R.string.south
            Forecastable.WindDirection.West -> R.string.west
            Forecastable.WindDirection.East -> R.string.east
            Forecastable.WindDirection.SouthWest -> R.string.south_west
            Forecastable.WindDirection.SouthEast -> R.string.south_east
            Forecastable.WindDirection.NorthWest -> R.string.north_west
            Forecastable.WindDirection.NorthEast -> R.string.north_east
        }
    )

fun City.Country.toString(context: Context): String =
    context.getString(
        when (this) {
            City.Country.RU -> R.string.country_ru
            City.Country.US -> R.string.country_usa
        }
    )

fun Int.toTempString(context: Context): String =
    context.getString(R.string.main_temp_pattern, this)