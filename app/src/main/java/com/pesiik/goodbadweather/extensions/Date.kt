package com.pesiik.goodbadweather.extensions

import android.text.format.DateUtils
import java.text.SimpleDateFormat
import java.util.*


val Long.fullDateString: String
    get() = SimpleDateFormat(
        "yyyy, dd MMMM, HH:mm",
        Locale.getDefault()
    ).format(
        Date(this)
    )

val Long.timeString: String
    get() = SimpleDateFormat(
        "HH:mm",
        Locale.getDefault()
    ).format(
        Date(this)
    )

val Long.dateString: String
    get() = SimpleDateFormat(
        "yyyy, dd MMMM",
        Locale.getDefault()
    ).format(
        Date(this)
    )

fun Long.isTooday() = DateUtils.isToday(this)