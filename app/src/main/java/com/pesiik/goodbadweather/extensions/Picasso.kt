package com.pesiik.goodbadweather.extensions

import android.widget.ImageView
import com.squareup.picasso.Picasso

private const val PATH_TO_ICON = "http://openweathermap.org/img/wn/"

fun ImageView.loadWeatherImage(url: String) =
    Picasso.get().load("${PATH_TO_ICON}$url.png").into(this)
