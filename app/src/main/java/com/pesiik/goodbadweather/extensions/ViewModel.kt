package com.pesiik.goodbadweather.extensions

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.*
import com.pesiik.entities.common.AppResult

inline fun <reified T : ViewModel> FragmentActivity.viewModel(
    factory: ViewModelProvider.Factory,
    body: T.() -> Unit
): T = ViewModelProviders.of(this, factory)[T::class.java].apply(body)


inline fun <reified T : ViewModel> Fragment.viewModel(
    factory: ViewModelProvider.Factory,
    body: T.() -> Unit
): T = ViewModelProviders.of(this, factory)[T::class.java].apply(body)


fun <T : Any, L : LiveData<T>> LifecycleOwner.observe(liveData: L, body: ((T?) -> Unit)? = null) =
    body?.let {
        liveData.observe(this, Observer(it))
    }


fun <L : LiveData<AppResult.Error>> LifecycleOwner.failureObserve(
    liveData: L,
    body: ((AppResult.Error?) -> Unit)? = null
) =
    body?.let {
        liveData.observe(this, Observer(it))
    }