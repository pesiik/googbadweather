package com.pesiik.goodbadweather.networkconnection

import android.content.Context
import com.pesiik.presentation.base.NetworkHandler
import kotlinx.coroutines.suspendCancellableCoroutine
import java.net.InetAddress
import kotlin.coroutines.resume

class NetworkHandlerImpl(private val context: Context) : NetworkHandler {
    override suspend fun isConnect(): Boolean = suspendCancellableCoroutine { coroutine ->
        coroutine.run {
            resume(
                try {
                    val inAddress= InetAddress.getByName("google.com");
                    !inAddress.equals("")
                } catch (e: Exception) {
                    false;
                }
            )
        }
    }
}