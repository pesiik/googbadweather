package com.pesiik.goodbadweather.sharedprefs

import android.content.Context
import javax.inject.Inject

class SharedPreferencesManager @Inject constructor(
    context: Context
) {

    val preferences = context.getSharedPreferences(WEATHER_PREFS, Context.MODE_PRIVATE)
    val defaultCityId: Int
        get() = preferences.getInt(CITY_ID, DEFAULT_CITY_ID)

    companion object {
        private const val WEATHER_PREFS = "weatherPREFS"

        const val CITY_ID = "cityId"
        const val DEFAULT_CITY_ID = -1
    }

}