package com.pesiik.goodbadweather

import android.os.Bundle
import android.view.MenuItem
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.pesiik.goodbadweather.ui.navigation.NavigationHandlerImpl
import com.pesiik.presentation.base.NavigationHandler
import com.pesiik.presentation.navigation.NavigationDestination
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private val navController by lazy { findNavController(R.id.nav_host_fragment) }
    private val navigationHandler: NavigationHandler by lazy { NavigationHandlerImpl(navController) }
    private val navHost by lazy { supportFragmentManager.findFragmentById(R.id.nav_host_fragment)!! }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val navView: BottomNavigationView = findViewById(R.id.navView)
        val appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.navigation_nearest_cities, R.id.navigation_my_city
            )
        )
        setSupportActionBar(mainToolbar.apply {
            setTitle(R.string.app_name)
        })
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.apply {
            itemIconTintList = null
        }.setupWithNavController(navController)

        onBackPressedDispatcher.addCallback(object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                if (navHost.childFragmentManager.backStackEntryCount == 0) {
                    finish()
                } else {
                    navigationHandler.navigateTo(NavigationDestination.Back)
                }
            }
        })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> onBackPressed()
        }
        return true
    }
}
