package com.pesiik.goodbadweather.ui.common

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import com.pesiik.goodbadweather.R

class ProgressDialog(context: Context) : Dialog(context, android.R.style.Widget_DeviceDefault_Light_ProgressBar) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_progress_bar)
        setCancelable(false)
    }
}