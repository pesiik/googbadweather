package com.pesiik.goodbadweather.ui.forecast

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.pesiik.entities.forecast.Forecast
import com.pesiik.goodbadweather.R
import com.pesiik.goodbadweather.extensions.dateString
import com.pesiik.goodbadweather.extensions.loadWeatherImage
import com.pesiik.goodbadweather.extensions.timeString
import kotlinx.android.synthetic.main.item_forecast.view.*
import kotlin.math.roundToInt

class ForecastViewHolder(
    view: View
) : RecyclerView.ViewHolder(
    view
) {

    fun onBind(forecast: Forecast, isHourly: Boolean) {
        with(itemView) {
            timeText.text =
                if (isHourly) {
                    forecast.date.timeString
                } else {
                    forecast.date.dateString
                }
            forecast.icon?.let { weatherThumbnail.loadWeatherImage(it) }
            forecast.humidity?.let {
                humidityText.text = context.getString(R.string.percentage_pattern, it)
            }
            temperatureForecastText.text =
                context.getString(R.string.main_temp_pattern, forecast.temperature?.roundToInt())
        }
    }


}