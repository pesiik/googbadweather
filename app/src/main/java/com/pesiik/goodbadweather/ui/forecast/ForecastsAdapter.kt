package com.pesiik.goodbadweather.ui.forecast

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.pesiik.entities.forecast.Forecast
import com.pesiik.goodbadweather.R

class ForecastsAdapter(private val isHourly: Boolean) : RecyclerView.Adapter<ForecastViewHolder>() {

    private val forecasts: MutableList<Forecast> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ForecastViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.item_forecast, parent, false)
    )

    override fun getItemCount() = forecasts.size

    override fun onBindViewHolder(holder: ForecastViewHolder, position: Int) {
        holder.onBind(forecasts[position], isHourly)
    }

    fun updateForecasts(list: List<Forecast>) {
        val diffUtilCallback = DiffUtil.calculateDiff(
            DiffUtilCallback(oldList = forecasts, newList = list),
            true
        )
        forecasts.clear()
        forecasts.addAll(list)
        diffUtilCallback.dispatchUpdatesTo(this)
    }

    private inner class DiffUtilCallback(
        private val oldList: List<Forecast>,
        private val newList: List<Forecast>
    ) : DiffUtil.Callback() {

        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int) = false

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int) =
            oldList[oldItemPosition].temperature == newList[newItemPosition].temperature &&
                    oldList[oldItemPosition].minTemperature == newList[newItemPosition].minTemperature &&
                    oldList[oldItemPosition].maxTemperature == newList[newItemPosition].maxTemperature &&
                    oldList[oldItemPosition].windSpeed == newList[newItemPosition].windSpeed &&
                    oldList[oldItemPosition].windDegrees == newList[newItemPosition].windDegrees &&
                    oldList[oldItemPosition].pressure == newList[newItemPosition].pressure

        override fun getOldListSize() = oldList.size

        override fun getNewListSize() = newList.size


    }
}