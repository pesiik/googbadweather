package com.pesiik.goodbadweather.ui.forecast

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.pesiik.entities.common.AppResult
import com.pesiik.entities.forecast.Forecast
import com.pesiik.goodbadweather.R
import com.pesiik.goodbadweather.extensions.makeGone
import com.pesiik.goodbadweather.extensions.makeVisible
import com.pesiik.goodbadweather.extensions.observe
import com.pesiik.goodbadweather.extensions.viewModel
import com.pesiik.goodbadweather.inject.application.AppComponent
import com.pesiik.goodbadweather.ui.common.BaseFragment
import com.pesiik.goodbadweather.ui.common.DividerWithoutLast
import com.pesiik.presentation.base.LoadingState
import com.pesiik.presentation.viewmodels.ForecastViewModel
import kotlinx.android.synthetic.main.fragment_forecast.*
import javax.inject.Inject


class ForecastFragment : BaseFragment() {
    override fun getLayoutRes() = R.layout.fragment_forecast

    @Inject
    override lateinit var viewModel: ForecastViewModel
    private lateinit var adapter: ForecastsAdapter

    private var loadingCallback: LoadingCallback? = null

    override val loadingObserver: Observer<LoadingState> =
        Observer { state ->
            if (state == LoadingState.LOADING) {
                forecastsRecycler.makeGone()
                forecastsProgressBar.makeVisible()
                noConnectionCityMessage.makeGone()
            } else {
                forecastsRecycler.makeVisible()
                forecastsProgressBar.makeGone()
                noConnectionCityMessage.makeGone()
            }
            loadingCallback?.onLoadingStateChange(state)
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AppComponent.get().inject(this)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        loadingCallback = parentFragment as LoadingCallback
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = viewModel(viewModelFactory) {
            observe(forecasts, ::renderForecasts)
            observe(failure, ::handleFailure)
        }
        forecastsRecycler.addItemDecoration(
            DividerWithoutLast(
                context!!,
                (forecastsRecycler.layoutManager as LinearLayoutManager).orientation
            )
        )
    }

    private fun renderForecasts(forecasts: List<Forecast>?) {
        forecasts?.apply {
            arguments?.getBoolean(ARG_IS_HOURLY)?.let {
                adapter = ForecastsAdapter(it)
                forecastsRecycler.adapter = adapter
                adapter.updateForecasts(this)
            }
        }
    }

    fun updateHourlyForecastsFromApi(cityId: Int) {
        arguments = Bundle().apply {
            putBoolean(ARG_IS_HOURLY, true)
        }
        viewModel.updateHourlyForecastsFromApi(cityId)
    }

    fun updateDailyForecastsFromApi(cityId: Int) {
        arguments = Bundle().apply {
            putBoolean(ARG_IS_HOURLY, false)
        }
        viewModel.updateDailyForecastsFromApi(cityId)
    }

    fun updateHourlyForecasts(cityId: Int) {
        arguments = Bundle().apply {
            putBoolean(ARG_IS_HOURLY, true)
        }
        viewModel.updateHourlyForecastsFromApi(cityId)
    }

    fun updateDailyForecasts(cityId: Int) {
        arguments = Bundle().apply {
            putBoolean(ARG_IS_HOURLY, true)
        }
        viewModel.updateDailyForecastsFromApi(cityId)
    }

    override fun handleFailure(failure: AppResult.Error?) {
        super.handleFailure(failure)
        failure?.let {
            if (it.reason == AppResult.Error.Reason.NO_CONNECTION) {
                noConnectionCityMessage.makeVisible()
            }
        }
    }

    companion object {
        private const val ARG_IS_HOURLY = "arg_is_hourly"
    }

    interface LoadingCallback {
        fun onLoadingStateChange(loadingState: LoadingState)
    }
}
