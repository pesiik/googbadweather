package com.pesiik.goodbadweather.ui.common

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.LayoutRes
import androidx.annotation.StringRes
import androidx.core.content.ContextCompat
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.pesiik.entities.common.AppResult
import com.pesiik.goodbadweather.R
import com.pesiik.goodbadweather.ui.navigation.NavigationHandlerImpl
import com.pesiik.presentation.base.BaseViewModel
import com.pesiik.presentation.base.LoadingState
import com.pesiik.presentation.base.NavigationHandler
import javax.inject.Inject

abstract class BaseFragment : DialogFragment() {

    @LayoutRes
    abstract fun getLayoutRes(): Int

    abstract val viewModel: BaseViewModel

    val navigationHandler: NavigationHandler by lazy { NavigationHandlerImpl(findNavController()) }

    @Inject
    protected lateinit var viewModelFactory: ViewModelProvider.Factory

    private val progressDialog by lazy { ProgressDialog(context!!) }
    open val loadingObserver by lazy {
        Observer<LoadingState> { state ->
            if (state == LoadingState.LOADING) {
                enableLoadbar(true)
            } else {
                enableLoadbar(false)
            }
        }
    }

    protected fun basedRenderFailure(@StringRes stringId: Int) {
        Toast.makeText(activity, stringId, Toast.LENGTH_LONG).show()
    }

    open fun handleFailure(failure: AppResult.Error?) {
        when (failure?.reason) {
            AppResult.Error.Reason.NO_CONNECTION -> basedRenderFailure(R.string.network_connection_error)
            AppResult.Error.Reason.SERVER_ERROR -> basedRenderFailure(R.string.server_error)
            AppResult.Error.Reason.CALL_PER_MINUTE -> basedRenderFailure(R.string.per_minute_error)
            AppResult.Error.Reason.LOCATION_ERROR -> basedRenderFailure(R.string.location_error)
            AppResult.Error.Reason.NO_PERMISSIONS -> basedRenderFailure(R.string.no_permissions)
            AppResult.Error.Reason.UNKNOWN -> basedRenderFailure(R.string.unknown_error)
            else -> {
            }
        }
        Log.e(TAG, failure?.message, failure?.cause)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(getLayoutRes(), container, false)


    override fun onStart() {
        super.onStart()
        viewModel.loadingState.observe(this, loadingObserver)
    }

    override fun onStop() {
        super.onStop()
        viewModel.loadingState.removeObserver(loadingObserver)
    }

    override fun onDetach() {
        super.onDetach()
        viewModel.failure.value = null
    }

    private fun enableLoadbar(isEnable: Boolean) =
        if (isEnable) progressDialog.show() else progressDialog.dismiss()

    protected fun postNoPermissionsError() {
        handleFailure(
            AppResult.Error(
                reason = AppResult.Error.Reason.NO_PERMISSIONS,
                message = "No message permissions"
            )
        )
    }

    protected fun requestLocationPermission(action: () -> Unit) {
        if (ContextCompat.checkSelfPermission(
                context!!,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
            && ContextCompat.checkSelfPermission(
                context!!,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            requestPermissions(
                arrayOf(
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ), PERMISSIONS_REQUEST_LOCATION
            )
        } else {
            action.invoke()
        }
    }

    companion object {
        private const val TAG = "BaseFragmentTAG"

        const val PERMISSIONS_REQUEST_LOCATION = 198
    }
}