package com.pesiik.goodbadweather.ui.citieslist

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.pesiik.entities.city.City
import com.pesiik.goodbadweather.R

class CitiesAdapter(
    private val onCityClick: OnCityClick
) : RecyclerView.Adapter<CityViewHolder>() {

    private val cities: MutableList<City> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = CityViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.item_city, parent, false),
        onCityClick
    )

    override fun getItemCount() = cities.size

    override fun onBindViewHolder(holder: CityViewHolder, position: Int) {
        holder.onBind(cities[position])
    }

    fun updateCities(list: List<City>) {
        val diffUtilCallback = DiffUtil.calculateDiff(
            DiffUtilCallback(oldList = cities, newList = list),
            true
        )
        cities.clear()
        cities.addAll(list)
        diffUtilCallback.dispatchUpdatesTo(this)
    }

    private inner class DiffUtilCallback(
        private val oldList: List<City>,
        private val newList: List<City>
    ) : DiffUtil.Callback() {

        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int) =
            oldList[oldItemPosition].id == newList[newItemPosition].id

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int) =
            oldList[oldItemPosition].temperature == newList[newItemPosition].temperature &&
                    oldList[oldItemPosition].minTemperature == newList[newItemPosition].minTemperature &&
                    oldList[oldItemPosition].maxTemperature == newList[newItemPosition].maxTemperature &&
                    oldList[oldItemPosition].windSpeed == newList[newItemPosition].windSpeed &&
                    oldList[oldItemPosition].windDegrees == newList[newItemPosition].windDegrees &&
                    oldList[oldItemPosition].pressure == newList[newItemPosition].pressure

        override fun getOldListSize() = oldList.size

        override fun getNewListSize() = newList.size


    }
}

typealias OnCityClick = (City) -> Unit