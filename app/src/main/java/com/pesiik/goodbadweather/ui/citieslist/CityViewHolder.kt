package com.pesiik.goodbadweather.ui.citieslist

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.pesiik.entities.city.City
import com.pesiik.goodbadweather.R
import com.pesiik.goodbadweather.extensions.loadWeatherImage
import com.pesiik.goodbadweather.extensions.setOnOneClickListener
import com.pesiik.goodbadweather.extensions.fullDateString
import com.pesiik.goodbadweather.extensions.toTempString
import kotlinx.android.synthetic.main.item_city.view.*
import kotlin.math.roundToInt

class CityViewHolder(
    view: View,
    private val onCityClick: OnCityClick
) : RecyclerView.ViewHolder(view) {

    fun onBind(city: City) {
        with(itemView) {
            cityTitle.text = city.name
            currentTemp.text = city.temperature?.roundToInt()?.toTempString(itemView.context)
            minMaxTemp.text = context.getString(
                R.string.min_max_temp_pattern,
                city.minTemperature?.roundToInt(),
                city.maxTemperature?.roundToInt()
            )
            date.text = city.date.fullDateString
            setOnOneClickListener {
                onCityClick.invoke(city)
            }
            city.icon?.let {
                weatherIcon.loadWeatherImage(it)
            }
        }
    }

}