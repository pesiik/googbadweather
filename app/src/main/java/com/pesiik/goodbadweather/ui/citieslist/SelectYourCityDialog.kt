package com.pesiik.goodbadweather.ui.citieslist

import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import com.pesiik.entities.city.City
import com.pesiik.goodbadweather.R
import com.pesiik.goodbadweather.extensions.observe
import com.pesiik.goodbadweather.extensions.setOnOneClickListener
import com.pesiik.goodbadweather.extensions.viewModel
import com.pesiik.goodbadweather.inject.application.AppComponent
import com.pesiik.goodbadweather.ui.common.BaseFragment
import com.pesiik.presentation.navigation.NavigationDestination
import com.pesiik.presentation.viewmodels.NearestCitiesViewModel
import kotlinx.android.synthetic.main.dialog_selected_your_city.view.*
import javax.inject.Inject


class SelectYourCityDialog : BaseFragment() {

    override fun getLayoutRes() = R.layout.dialog_selected_your_city

    @Inject
    override lateinit var viewModel: NearestCitiesViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AppComponent.get().inject(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = activity!!.viewModel(viewModelFactory) {
            observe(nearestCity, ::renderCity)
        }
        view.yesButton.setOnOneClickListener {
            navigationHandler.navigateTo(NavigationDestination.Back)
            viewModel.isDefaultCityUpdated = true
            viewModel.updateDefaultCity(viewModel.nearestCity.value!!)
            viewModel.selectCity()
            viewModel.nearestCity.value = null
            dialog!!.dismiss()
        }
        view.noButton.setOnOneClickListener {
            navigationHandler.navigateTo(NavigationDestination.Back)
            dialog!!.dismiss()
        }
    }

    override fun onResume() {
        super.onResume()
        val params = dialog!!.window!!.attributes
        params.width = ViewGroup.LayoutParams.MATCH_PARENT
        dialog!!.window!!.attributes = params as android.view.WindowManager.LayoutParams
    }

    private fun renderCity(city: City?) {
        city?.let {
            view!!.chooseTitle.text = getString(R.string.choose_your_city_title, it.name)
        }
    }
}