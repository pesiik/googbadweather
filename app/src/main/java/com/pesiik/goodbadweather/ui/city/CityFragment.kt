package com.pesiik.goodbadweather.ui.city

import android.content.pm.PackageManager
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import com.pesiik.entities.city.City
import com.pesiik.entities.common.AppResult
import com.pesiik.goodbadweather.R
import com.pesiik.goodbadweather.extensions.*
import com.pesiik.goodbadweather.inject.application.AppComponent
import com.pesiik.goodbadweather.ui.common.BaseFragment
import com.pesiik.goodbadweather.ui.forecast.ForecastFragment
import com.pesiik.presentation.base.LoadingState
import com.pesiik.presentation.viewmodels.CityViewModel
import kotlinx.android.synthetic.main.fragment_city.*
import javax.inject.Inject
import kotlin.math.roundToInt

class CityFragment : BaseFragment(), ForecastFragment.LoadingCallback {

    override fun getLayoutRes() = R.layout.fragment_city

    @Inject
    override lateinit var viewModel: CityViewModel

    override val loadingObserver: Observer<LoadingState> = Observer { state ->
        cityDetailSwipeRefreshLayout.isRefreshing = state != LoadingState.DONE
        emptyCityBlockResfreshLayout.isRefreshing = state != LoadingState.DONE
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AppComponent.get().inject(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = viewModel(viewModelFactory) {
            observe(cityLiveData, ::renderCity)
            observe(isEmpty, ::checkEmpty)
            observe(failure, ::handleFailure)
        }
        requestLocationPermission(this::updateCity)
        emptyCityBlockResfreshLayout.setOnRefreshListener {
            requestLocationPermission(this::updateCity)
        }
    }

    override fun onLoadingStateChange(loadingState: LoadingState) {
        loadingObserver.onChanged(loadingState)
    }

    private fun renderCity(city: City?) {
        city?.let { c ->
            with(context!!) {
                cityToolbar.title = city.name
                countryTitle.text = city.country?.toString(this)
                weatherDate.text = city.date.fullDateString
                weatherBigImage.loadWeatherImage(c.icon!!)
                mainTemp.text = city.temperature?.roundToInt()?.toTempString(this)
                minMaxTempFull.text = getString(
                    R.string.min_max_temp_pattern,
                    city.minTemperature?.roundToInt(),
                    city.maxTemperature?.roundToInt()
                )
                windDirectionValue.text = try {
                    city.getDirection().toString(this)
                } catch (e: IllegalStateException) {
                    getString(R.string.unknown_value)
                }
                humidityValue.text = getString(R.string.percentage_pattern, city.humidity)
                windSpeedValue.text = getString(R.string.wind_speed_pattern, city.windSpeed)
            }

            val hourlyFragment =
                (childFragmentManager.findFragmentByTag(
                    resources.getText(R.string.hourly_fragment_tag).toString()
                ) as ForecastFragment)

            val dailyFragment =
                (childFragmentManager.findFragmentByTag(
                    resources.getText(R.string.daily_fragment_tag).toString()
                ) as ForecastFragment)

            cityDetailSwipeRefreshLayout.setOnRefreshListener {
                hourlyFragment.updateHourlyForecastsFromApi(c.id)
                dailyFragment.updateDailyForecastsFromApi(c.id)
            }

            hourlyFragment.updateHourlyForecasts(c.id)
            dailyFragment.updateDailyForecasts(c.id)
        }
    }

    private fun updateCity() {
        arguments?.getInt(getString(R.string.arg_city_id))?.let { cityId ->
            viewModel.updateCityById(cityId)
        } ?: run {
            viewModel.getDefaultCity()
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            PERMISSIONS_REQUEST_LOCATION -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    updateCity()
                } else {
                    postNoPermissionsError()
                }
                return
            }
            else -> super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }

    private fun checkEmpty(isEmpty: Boolean?) {
        isEmpty?.let {
            if (isEmpty) {
                emptyCityBlockResfreshLayout.makeVisible()
                cityBlock.makeGone()
            } else {
                emptyCityBlockResfreshLayout.makeGone()
                cityBlock.makeVisible()
            }
        }
    }

    override fun handleFailure(failure: AppResult.Error?) {
        super.handleFailure(failure)
        if (failure?.reason == AppResult.Error.Reason.NO_PERMISSIONS) {
            cityBlock.makeGone()
            emptyCityBlockResfreshLayout.makeVisible()
        }
    }
}