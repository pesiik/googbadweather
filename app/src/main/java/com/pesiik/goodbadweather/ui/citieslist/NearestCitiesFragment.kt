package com.pesiik.goodbadweather.ui.citieslist

import android.content.pm.PackageManager
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.pesiik.entities.city.City
import com.pesiik.entities.common.AppResult
import com.pesiik.goodbadweather.R
import com.pesiik.goodbadweather.extensions.*
import com.pesiik.goodbadweather.inject.application.AppComponent
import com.pesiik.goodbadweather.ui.common.BaseFragment
import com.pesiik.goodbadweather.ui.common.DividerWithoutLast
import com.pesiik.presentation.base.LoadingState
import com.pesiik.presentation.navigation.NavigationDestination
import com.pesiik.presentation.viewmodels.NearestCitiesViewModel
import kotlinx.android.synthetic.main.fragment_nearest_cities.*
import javax.inject.Inject

class NearestCitiesFragment : BaseFragment(), OnCityClick {

    override fun getLayoutRes() = R.layout.fragment_nearest_cities

    @Inject
    override lateinit var viewModel: NearestCitiesViewModel

    override val loadingObserver: Observer<LoadingState> = Observer { state ->
        citiesRefreshLayout.isRefreshing = state == LoadingState.LOADING
    }

    private val citiesAdapter by lazy { CitiesAdapter(this) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AppComponent.get().inject(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = activity!!.viewModel(viewModelFactory) {
            observe(cities, ::renderCities)
            observe(nearestCity, ::onSetNearestCity)
            observe(selectedCity, ::onSelectCity)
            observe(failure, ::handleFailure)
        }
        citiesRecycler.adapter = citiesAdapter
        requestLocationPermission(this::updateCities)
        citiesRefreshLayout.setOnRefreshListener {
            citiesRecycler.addItemDecoration(
                DividerWithoutLast(
                    context!!,
                    (citiesRecycler.layoutManager as LinearLayoutManager).orientation
                )
            )
            if (viewModel.cities.value.isNullOrEmpty()) {
                requestLocationPermission(this::updateCities)
            }
            citiesRefreshLayout.setOnRefreshListener {
                requestLocationPermission(this::updateCities)
            }
            myCity.setOnOneClickListener {
                viewModel.isMyCityButtonClick = true
                viewModel.updateNearestCity()
            }
        }
    }

    private fun updateCities() {
        viewModel.updateNearestCities()
    }

    private fun renderCities(cities: List<City>?) {
        cities?.let {
            citiesAdapter.updateCities(it)
            citiesListBlock.makeVisible()
            noPermissionsBlock.makeGone()
            noDataBlock.makeGone()
        }
        viewModel.updateNearestCity()
    }

    override fun handleFailure(failure: AppResult.Error?) {
        super.handleFailure(failure)
        if (failure?.reason == AppResult.Error.Reason.NO_PERMISSIONS) {
            noPermissionsBlock.makeVisible()
            citiesListBlock.makeGone()
            noConnectionBlock.makeGone()
            noDataBlock.makeGone()
        }
        if (failure?.reason == AppResult.Error.Reason.NO_CONNECTION) {
            noConnectionBlock.makeVisible()
            noPermissionsBlock.makeGone()
            citiesListBlock.makeGone()
            noDataBlock.makeGone()
        }
    }

    private fun onSetNearestCity(city: City?) {
        city?.let {
            if (viewModel.isDefaultCityUpdated) {
                navigationHandler.navigateTo(NavigationDestination.FromListToConfirm)
            }
        }
    }

    private fun onSelectCity(city: City?) {
        city?.let {
            if (viewModel.isDefaultCityUpdated || viewModel.isMyCityButtonClick) {
                navigationHandler.navigateTo(
                    NavigationDestination.FromListToMyCity,
                    Bundle().apply { putInt(getString(R.string.arg_city_id), it.id) })
                viewModel.isDefaultCityUpdated = false
                viewModel.isMyCityButtonClick = false
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            PERMISSIONS_REQUEST_LOCATION -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    updateCities()
                } else {
                    postNoPermissionsError()
                }
                return
            }
            else -> super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }

    override fun invoke(city: City) {
        navigationHandler.navigateTo(NavigationDestination.FromListToOtherCity, Bundle().apply {
            putInt(getString(R.string.arg_city_id), city.id)
        })
    }
}