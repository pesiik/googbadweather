package com.pesiik.goodbadweather.ui.navigation

import android.os.Bundle
import androidx.navigation.NavController
import com.pesiik.goodbadweather.R
import com.pesiik.presentation.base.NavigationHandler
import com.pesiik.presentation.navigation.NavigationDestination

class NavigationHandlerImpl(
    private val navController: NavController
) : NavigationHandler {

    override fun navigateTo(navigationDestination: NavigationDestination, bundle: Bundle?) {
        when (navigationDestination) {
            NavigationDestination.Back -> navController.navigateUp()
            NavigationDestination.FromListToMyCity -> {
                if (navController.currentDestination?.id != R.id.navigation_my_city) {
                    navController.navigate(
                        R.id.action_from_list_to_my_city,
                        bundle!!
                    )
                }
            }
            NavigationDestination.FromListToOtherCity -> {
                if (navController.currentDestination?.id != R.id.navigation_other_city) {
                    navController.navigate(
                        R.id.action_from_list_to_other_city,
                        bundle!!
                    )
                }
            }
            NavigationDestination.FromListToConfirm -> {
                if (navController.currentDestination?.id != R.id.navigation_confirm_city) {
                    navController.navigate(R.id.action_from_list_to_confirm)
                }
            }
        }
    }
}