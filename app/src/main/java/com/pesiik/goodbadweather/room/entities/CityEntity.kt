package com.pesiik.goodbadweather.room.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import com.pesiik.goodbadweather.room.entities.CityEntity.Companion.TABLE_NAME

@Entity(
    tableName = TABLE_NAME,
    indices = [
        Index(
            CityEntity.ID,
            unique = true
        )
    ]
)
data class CityEntity(
    @PrimaryKey
    @ColumnInfo(name = ID)
    val id: Int,
    val name: String,
    val image: String?,
    val lat: Double,
    val lon: Double,
    val temperature: Float?,
    val pressure: Int?,
    val humidity: Int?,
    val minTemperature: Float?,
    val maxTemperature: Float?,
    val windSpeed: Int?,
    val windDegrees: Int?,
    val date: Long,
    val clouds: Int?,
    val country: String?
) {

    companion object {
        const val TABLE_NAME = "Cities"

        const val ID = "id"
    }
}