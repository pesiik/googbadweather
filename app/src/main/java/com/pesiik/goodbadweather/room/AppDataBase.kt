package com.pesiik.goodbadweather.room

import androidx.room.Database
import androidx.room.RoomDatabase
import com.pesiik.goodbadweather.room.AppDataBase.Companion.DB_VERSION
import com.pesiik.goodbadweather.room.dao.CityDao
import com.pesiik.goodbadweather.room.entities.CityEntity

@Database(
    entities = [CityEntity::class],
    version = DB_VERSION
)
abstract class AppDataBase : RoomDatabase() {
    abstract fun cityDao(): CityDao

    companion object {
        const val DB_NAME = "app_db_name"
        const val DB_VERSION = 1
    }
}