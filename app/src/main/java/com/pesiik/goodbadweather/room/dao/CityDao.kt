package com.pesiik.goodbadweather.room.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.pesiik.goodbadweather.room.entities.CityEntity

@Dao
interface CityDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCities(cityEntities: List<CityEntity>)

    @Query(
        """
            SELECT *
            FROM ${CityEntity.TABLE_NAME}
        """
    )
    fun getAllCities(): List<CityEntity>

    @Query(
        """
            SELECT * FROM ${CityEntity.TABLE_NAME} WHERE ${CityEntity.ID} == :id
        """
    )
    fun getCityById(id: Int): CityEntity?

}