package com.pesiik.goodbadweather.interactors

import com.pesiik.entities.city.City
import com.pesiik.entities.common.AppResult
import com.pesiik.interactor.interactors.NearestCitiesInteractor
import com.pesiik.interactor.location.LocationHandler
import com.pesiik.interactor.repositories.CityRepository
import javax.inject.Inject

class NearestCitiesInteractorImpl @Inject constructor(
    private val cityRepository: CityRepository,
    private val locationHandler: LocationHandler
) : NearestCitiesInteractor {

    override suspend fun getNearestCities(): AppResult<List<City>> {
        val myLocation = locationHandler.getMyLocation()
        return if (myLocation is AppResult.Success) {
            val cities = cityRepository.getNearestCities(
                lon = myLocation.value.lon,
                lat = myLocation.value.lat
            )
            if (cities is AppResult.Success) {
                cityRepository.insertCities(cities.value)
                AppResult.Success(cities.value)
            } else {
                cities as AppResult.Error
            }
        } else {
            myLocation as AppResult.Error
        }
    }

    override suspend fun getNearestCity(): AppResult<City> {
        val location = locationHandler.getMyLocation()
        return if (location is AppResult.Success) {
            cityRepository.getCityByGeolocation(
                lon = location.value.lon,
                lat = location.value.lat
            )
        } else {
            location as AppResult.Error
        }
    }
}
