package com.pesiik.goodbadweather.interactors

import com.pesiik.entities.city.City
import com.pesiik.entities.common.AppResult
import com.pesiik.interactor.interactors.CityInteractor
import com.pesiik.interactor.repositories.CityRepository
import javax.inject.Inject

class CityInteractorImpl @Inject constructor(
    private val cityRepository: CityRepository
) : CityInteractor {

    override suspend fun getCityById(id: Int): AppResult<City> =
        cityRepository.getCityDataById(id)

    override suspend fun isItDefaultCity(city: City): AppResult<Boolean> {
        val default = getDefaultCity()
        return if (default is AppResult.Success) {
            AppResult.Success(default.value.id == city.id)
        } else {
            default as AppResult.Error
        }
    }

    override suspend fun updateDefaultCity(city: City) = cityRepository.updateDefaultCity(city)

    override suspend fun getDefaultCity(): AppResult<City> = cityRepository.getDefaultCity()

    override suspend fun getCitiesFromDb(): AppResult<List<City>> = cityRepository.getCitiesFromDb()


}