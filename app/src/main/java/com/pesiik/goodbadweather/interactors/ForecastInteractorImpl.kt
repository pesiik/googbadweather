package com.pesiik.goodbadweather.interactors

import com.pesiik.entities.common.AppResult
import com.pesiik.entities.forecast.Forecast
import com.pesiik.goodbadweather.extensions.isTooday
import com.pesiik.interactor.interactors.ForecastInteractor
import com.pesiik.interactor.repositories.ForecastRepository
import javax.inject.Inject

class ForecastInteractorImpl @Inject constructor(
    private val forecastRepository: ForecastRepository
) : ForecastInteractor {

    override suspend fun getHourlyForecastByCityIdFromRest(cityId: Int): AppResult<List<Forecast>> {
        val result = forecastRepository.getForecastsByCityIdFromRest(cityId)
        return if (result is AppResult.Success) {
            AppResult.Success(
                result.value.filterIndexed { index, forecast ->
                    forecast.date.isTooday() || index < 7
                }
            )
        } else {
            AppResult.Error(
                AppResult.Error.Reason.SERVER_ERROR,
                message = "getHourlyForecastByCityId error"
            )
        }
    }

    override suspend fun getDailyForecastByCityIdFromRest(cityId: Int): AppResult<List<Forecast>> {
        val result = forecastRepository.getForecastsByCityIdFromRest(cityId)
        return if (result is AppResult.Success) {
            AppResult.Success(
                result.value.filterIndexed { index, _ ->
                    index % 7 == 0
                }
            )
        } else {
            AppResult.Error(
                AppResult.Error.Reason.SERVER_ERROR,
                message = "getDailyForecastByCityId error"
            )
        }
    }
}