package com.pesiik.goodbadweather.inject.interactors

import com.pesiik.goodbadweather.inject.base.BaseComponentHolder
import com.pesiik.goodbadweather.inject.data.DataComponent
import com.pesiik.goodbadweather.inject.data.DataDependencies
import com.pesiik.goodbadweather.inject.location.LocationComponent
import com.pesiik.goodbadweather.inject.location.LocationDependencies
import dagger.Component

@Component(
    modules = [InteractorsModule::class],
    dependencies = [
        DataDependencies::class,
        LocationDependencies::class
    ]
)
interface InteractorsComponent : InteractorsDependencies {
    companion object : BaseComponentHolder<InteractorsComponent>() {
        override fun provide(): InteractorsComponent =
            DaggerInteractorsComponent.builder()
                .dataDependencies(DataComponent.get())
                .locationDependencies(LocationComponent.get())
                .build()
    }
}