package com.pesiik.goodbadweather.inject.location

import android.content.Context
import com.pesiik.goodbadweather.location.LocationHandlerImpl
import com.pesiik.interactor.location.LocationHandler
import dagger.Module
import dagger.Provides

@Module
class LocationModule {

    @Provides
    fun provideLocationHandler(context: Context): LocationHandler =
        LocationHandlerImpl(context)

}