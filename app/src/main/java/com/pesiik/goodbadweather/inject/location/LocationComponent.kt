package com.pesiik.goodbadweather.inject.location

import com.pesiik.goodbadweather.inject.base.BaseComponentHolder
import com.pesiik.goodbadweather.inject.context.ContextComponent
import com.pesiik.goodbadweather.inject.context.ContextDependencies
import dagger.Component

@Component(
    modules = [LocationModule::class],
    dependencies = [ContextDependencies::class]
)
interface LocationComponent : LocationDependencies {
    companion object : BaseComponentHolder<LocationComponent>() {
        override fun provide(): LocationComponent =
            DaggerLocationComponent.builder()
                .contextDependencies(ContextComponent.get())
                .build()
    }
}