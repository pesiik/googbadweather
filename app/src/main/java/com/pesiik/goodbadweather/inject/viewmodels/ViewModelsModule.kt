package com.pesiik.goodbadweather.inject.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.pesiik.presentation.viewmodels.CityViewModel
import com.pesiik.presentation.viewmodels.ForecastViewModel
import com.pesiik.presentation.viewmodels.NearestCitiesViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelsModule {
    @Binds
    internal abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(CityViewModel::class)
    abstract fun bindsCityViewModel(cityViewModel: CityViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(NearestCitiesViewModel::class)
    abstract fun bindsNearestCitiesViewModel(nearestCitiesViewModel: NearestCitiesViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ForecastViewModel::class)
    abstract fun bindsForecastViewModel(forecastViewModel: ForecastViewModel): ViewModel
}