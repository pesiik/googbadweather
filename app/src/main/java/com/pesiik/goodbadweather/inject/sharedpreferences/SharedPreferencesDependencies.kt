package com.pesiik.goodbadweather.inject.sharedpreferences

import com.pesiik.goodbadweather.sharedprefs.SharedPreferencesManager

interface SharedPreferencesDependencies {
    fun provideSharedPreferencesManager(): SharedPreferencesManager
}