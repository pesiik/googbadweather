package com.pesiik.goodbadweather.inject.network

import com.pesiik.goodbadweather.inject.base.BaseComponentHolder
import com.pesiik.goodbadweather.inject.context.ContextComponent
import com.pesiik.goodbadweather.inject.context.ContextDependencies
import dagger.Component

@Component(
    modules = [NetworkModule::class],
    dependencies = [ContextDependencies::class]
)
interface NetworkComponent : NetworkDependencies {
    companion object : BaseComponentHolder<NetworkComponent>(){
        override fun provide(): NetworkComponent =
            DaggerNetworkComponent.builder()
                .networkModule(NetworkModule())
                .contextDependencies(ContextComponent.get())
                .build()
    }
}