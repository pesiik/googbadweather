package com.pesiik.goodbadweather.inject.retrofit

import com.pesiik.goodbadweather.inject.manager.ApiManager
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor

@Module
class ApiModule {
    @Provides
    fun provideApiManager(): ApiManager = ApiManager(
        OkHttpClient()
            .newBuilder()
            .addInterceptor(HttpLoggingInterceptor())
            .build()
    )
}