package com.pesiik.goodbadweather.inject.interactors

import com.pesiik.goodbadweather.interactors.CityInteractorImpl
import com.pesiik.goodbadweather.interactors.ForecastInteractorImpl
import com.pesiik.goodbadweather.interactors.NearestCitiesInteractorImpl
import com.pesiik.interactor.interactors.CityInteractor
import com.pesiik.interactor.interactors.ForecastInteractor
import com.pesiik.interactor.interactors.NearestCitiesInteractor
import com.pesiik.interactor.location.LocationHandler
import com.pesiik.interactor.repositories.CityRepository
import com.pesiik.interactor.repositories.ForecastRepository
import dagger.Module
import dagger.Provides

@Module
class InteractorsModule {

    @Provides
    fun provideNearestCitiesInteractor(
        cityRepository: CityRepository,
        locationHandler: LocationHandler
    ): NearestCitiesInteractor =
        NearestCitiesInteractorImpl(
            cityRepository,
            locationHandler
        )

    @Provides
    fun provideCityInteractor(cityRepository: CityRepository): CityInteractor =
        CityInteractorImpl(cityRepository)

    @Provides
    fun provideForecastInteractor(
        forecastRepository: ForecastRepository
    ): ForecastInteractor =
        ForecastInteractorImpl(
            forecastRepository
        )
}