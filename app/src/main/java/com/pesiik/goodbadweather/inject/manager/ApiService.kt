package com.pesiik.goodbadweather.inject.manager

import com.pesiik.entities.city.CitiesRootObject
import com.pesiik.entities.city.City
import com.pesiik.entities.forecast.ForecastRootObject
import com.pesiik.goodbadweather.BuildConfig
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {

    @GET("weather")
    fun getCityByGeolocation(
        @Query("lon") lon: Double,
        @Query("lat") lat: Double,
        @Query("units") units: String = "metric",
        @Query("APPID") key: String = BuildConfig.KEY
    ): Call<City>

    @GET("find")
    fun getCitiesAroundGeolocation(
        @Query("lon") lon: Double,
        @Query("lat") lat: Double,
        @Query("cnt") cnt: Int = 50,
        @Query("units") units: String = "metric",
        @Query("APPID") key: String = BuildConfig.KEY
    ): Call<CitiesRootObject>

    @GET("forecast")
    fun getForecastsByCityId(
        @Query("id") cityId: Int,
        @Query("units") units: String = "metric",
        @Query("APPID") key: String = BuildConfig.KEY
    ): Call<ForecastRootObject>

}