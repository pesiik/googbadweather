package com.pesiik.goodbadweather.inject.sharedpreferences

import android.content.Context
import com.pesiik.goodbadweather.sharedprefs.SharedPreferencesManager
import dagger.Module
import dagger.Provides

@Module
class SharedPreferencesModule {

    @Provides
    fun provideSharedPreferencesManager(context: Context): SharedPreferencesManager =
        SharedPreferencesManager(context)

}