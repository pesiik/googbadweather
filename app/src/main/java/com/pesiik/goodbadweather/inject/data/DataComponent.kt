package com.pesiik.goodbadweather.inject.data

import com.pesiik.goodbadweather.inject.base.BaseComponentHolder
import com.pesiik.goodbadweather.inject.context.ContextComponent
import com.pesiik.goodbadweather.inject.context.ContextDependencies
import com.pesiik.goodbadweather.inject.retrofit.ApiComponent
import com.pesiik.goodbadweather.inject.retrofit.ApiDependencies
import com.pesiik.goodbadweather.inject.room.RoomComponent
import com.pesiik.goodbadweather.inject.room.RoomDependencies
import dagger.Component

@Component(
    modules = [DataModule::class],
    dependencies = [ContextDependencies::class, ApiDependencies::class, RoomDependencies::class]
)
interface DataComponent : DataDependencies {
    companion object : BaseComponentHolder<DataComponent>() {
        override fun provide(): DataComponent =
            DaggerDataComponent.builder()
                .dataModule(DataModule())
                .contextDependencies(ContextComponent.get())
                .apiDependencies(ApiComponent.get())
                .roomDependencies(RoomComponent.get())
                .build()
    }
}