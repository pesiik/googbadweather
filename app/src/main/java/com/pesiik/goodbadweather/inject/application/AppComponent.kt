package com.pesiik.goodbadweather.inject.application

import com.pesiik.goodbadweather.inject.base.BaseComponentHolder
import com.pesiik.goodbadweather.inject.interactors.InteractorsComponent
import com.pesiik.goodbadweather.inject.interactors.InteractorsDependencies
import com.pesiik.goodbadweather.inject.network.NetworkComponent
import com.pesiik.goodbadweather.inject.network.NetworkDependencies
import com.pesiik.goodbadweather.inject.room.RoomComponent
import com.pesiik.goodbadweather.inject.room.RoomDependencies
import com.pesiik.goodbadweather.inject.viewmodels.ViewModelsModule
import com.pesiik.goodbadweather.ui.citieslist.NearestCitiesFragment
import com.pesiik.goodbadweather.ui.citieslist.SelectYourCityDialog
import com.pesiik.goodbadweather.ui.city.CityFragment
import com.pesiik.goodbadweather.ui.forecast.ForecastFragment
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(
    modules = [ViewModelsModule::class],
    dependencies = [InteractorsDependencies::class, RoomDependencies::class, NetworkDependencies::class]
)
interface AppComponent {
    companion object : BaseComponentHolder<AppComponent>() {
        override fun provide(): AppComponent =
            DaggerAppComponent.builder()
                .interactorsDependencies(InteractorsComponent.get())
                .roomDependencies(RoomComponent.get())
                .networkDependencies(NetworkComponent.get())
                .build()
    }

    fun inject(cityFragment: CityFragment)
    fun inject(nearestCitiesFragment: NearestCitiesFragment)
    fun inject(selectYourCityDialog: SelectYourCityDialog)
    fun inject(forecastFragment: ForecastFragment)
}