package com.pesiik.goodbadweather.inject.room

import com.pesiik.goodbadweather.room.AppDataBase

interface RoomDependencies {
    fun provideRoomDataBase(): AppDataBase
}