package com.pesiik.goodbadweather.inject.retrofit.deserializers

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.pesiik.entities.city.City
import com.pesiik.entities.common.Forecastable
import com.pesiik.entities.forecast.Forecast
import java.lang.reflect.Type

class ForecastableDeserializer : JsonDeserializer<Forecastable> {
    override fun deserialize(
        json: JsonElement,
        typeOfT: Type,
        context: JsonDeserializationContext
    ): Forecastable {
        val jsonObj = json.asJsonObject
        val jsonCoord = jsonObj["coord"]?.asJsonObject
        val jsonMain = jsonObj["main"].asJsonObject
        val jsonWind = jsonObj["wind"].asJsonObject
        val cityId = jsonObj["id"]?.asInt
        return if (cityId != null) {
             City(
                id = jsonObj["id"].asInt,
                lon = jsonCoord?.get("lon")!!.asDouble,
                lat = jsonCoord.get("lat")!!.asDouble,
                icon = jsonObj["weather"].asJsonArray[0].asJsonObject["icon"]?.asString,
                clouds = jsonObj["clouds"].asJsonObject["all"]?.asInt,
                name = jsonObj["name"].asString,
                temperature = jsonMain["temp"]?.asFloat,
                pressure = jsonMain["pressure"]?.asInt,
                humidity = jsonMain["humidity"]?.asInt,
                minTemperature = jsonMain["temp_min"]?.asFloat,
                maxTemperature = jsonMain["temp_max"]?.asFloat,
                date = jsonObj["dt"].asLong * 1000,
                windSpeed = jsonWind["speed"]?.asInt,
                windDegrees = jsonWind["deg"]?.asInt,
                country = City.Country.valueOf(jsonObj["sys"].asJsonObject["country"].asString)
            )
        } else {
            Forecast(
                icon = jsonObj["weather"].asJsonArray[0].asJsonObject["icon"]?.asString,
                clouds = jsonObj["clouds"].asJsonObject["all"]?.asInt,
                temperature = jsonMain["temp"]?.asFloat,
                pressure = jsonMain["pressure"]?.asInt,
                humidity = jsonMain["humidity"]?.asInt,
                minTemperature = jsonMain["temp_min"]?.asFloat,
                maxTemperature = jsonMain["temp_max"]?.asFloat,
                date = jsonObj["dt"].asLong * 1000,
                windSpeed = jsonWind["speed"]?.asInt,
                windDegrees = jsonWind["deg"]?.asInt
            )
        }
    }
}