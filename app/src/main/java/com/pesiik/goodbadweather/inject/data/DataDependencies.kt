package com.pesiik.goodbadweather.inject.data

import com.pesiik.interactor.repositories.CityRepository
import com.pesiik.interactor.repositories.ForecastRepository

interface DataDependencies {
    fun provideCityRepository(): CityRepository
    fun provideForecastRepository(): ForecastRepository
}