package com.pesiik.goodbadweather.inject.sharedpreferences

import com.pesiik.goodbadweather.inject.base.BaseComponentHolder
import com.pesiik.goodbadweather.inject.context.ContextComponent
import com.pesiik.goodbadweather.inject.context.ContextDependencies
import dagger.Component

@Component(
    modules = [SharedPreferencesModule::class],
    dependencies = [ContextDependencies::class]
)
interface SharedPreferencesComponent : SharedPreferencesDependencies {
    companion object : BaseComponentHolder<SharedPreferencesComponent>() {
        override fun provide(): SharedPreferencesComponent =
            DaggerSharedPreferencesComponent.builder()
                .contextDependencies(ContextComponent.get())
                .build()
    }
}