package com.pesiik.goodbadweather.inject.context

import android.content.Context
import com.pesiik.goodbadweather.inject.base.BaseAppContextHolder
import dagger.Component

@Component(modules = [ContextModule::class])
interface ContextComponent : ContextDependencies {
    companion object : BaseAppContextHolder<ContextComponent>() {
        override fun provide(context: Context): ContextComponent {
            return DaggerContextComponent.builder()
                .contextModule(ContextModule(context))
                .build()
        }
    }
}