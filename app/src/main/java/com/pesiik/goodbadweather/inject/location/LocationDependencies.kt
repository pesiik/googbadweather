package com.pesiik.goodbadweather.inject.location

import com.pesiik.interactor.location.LocationHandler

interface LocationDependencies {
    fun provideLocationHandler(): LocationHandler
}