package com.pesiik.goodbadweather.inject.interactors

import com.pesiik.interactor.interactors.CityInteractor
import com.pesiik.interactor.interactors.ForecastInteractor
import com.pesiik.interactor.interactors.NearestCitiesInteractor

interface InteractorsDependencies {
    fun provideNearestCitiesInteractor(): NearestCitiesInteractor
    fun provideCityInteractor(): CityInteractor
    fun provideForecastInteractor(): ForecastInteractor
}