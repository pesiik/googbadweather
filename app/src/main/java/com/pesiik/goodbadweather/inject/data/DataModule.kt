package com.pesiik.goodbadweather.inject.data

import com.pesiik.goodbadweather.inject.manager.ApiManager
import com.pesiik.goodbadweather.repositories.CityRepositoryImpl
import com.pesiik.goodbadweather.repositories.ForecastRepositoryImpl
import com.pesiik.goodbadweather.room.AppDataBase
import com.pesiik.goodbadweather.sharedprefs.SharedPreferencesManager
import com.pesiik.interactor.repositories.CityRepository
import com.pesiik.interactor.repositories.ForecastRepository
import dagger.Module
import dagger.Provides

@Module
class DataModule {

    @Provides
    fun provideCityRepository(
        apiManager: ApiManager,
        database: AppDataBase,
        sharedPreferencesManager: SharedPreferencesManager
    ): CityRepository =
        CityRepositoryImpl(apiManager, database, sharedPreferencesManager)

    @Provides
    fun provideForecastRepository(
        apiManager: ApiManager,
        database: AppDataBase
    ): ForecastRepository = ForecastRepositoryImpl(apiManager, database)
}