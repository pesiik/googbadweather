package com.pesiik.goodbadweather.inject.base

import android.content.Context
import java.lang.ref.WeakReference

abstract class BaseAppContextHolder<C : Any> {

    private var component: WeakReference<C>? = null
    private lateinit var provider: () -> C

    private val syncObject = Any()

    fun get(): C {
        var componentVal = component?.get()
        if (componentVal == null) {
            synchronized(syncObject) {
                componentVal = provider.invoke()
                component = WeakReference(componentVal!!)
            }
        }
        return componentVal!!
    }

    abstract fun provide(context: Context): C

    fun initProvider(provider: () -> C) {
        this.provider = provider
    }
}