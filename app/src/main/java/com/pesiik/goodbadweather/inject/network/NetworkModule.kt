package com.pesiik.goodbadweather.inject.network

import android.content.Context
import com.pesiik.goodbadweather.networkconnection.NetworkHandlerImpl
import com.pesiik.presentation.base.NetworkHandler
import dagger.Module
import dagger.Provides

@Module
class NetworkModule {

    @Provides
    fun provideNetworkHandler(context: Context): NetworkHandler =
        NetworkHandlerImpl(context)

}