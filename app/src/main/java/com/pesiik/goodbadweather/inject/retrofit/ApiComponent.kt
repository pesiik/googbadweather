package com.pesiik.goodbadweather.inject.retrofit

import com.pesiik.goodbadweather.inject.base.BaseComponentHolder
import com.pesiik.goodbadweather.inject.context.ContextComponent
import com.pesiik.goodbadweather.inject.context.ContextDependencies
import dagger.Component

@Component(
    modules = [ApiModule::class],
    dependencies = [ContextDependencies::class]
)
interface ApiComponent : ApiDependencies {
    companion object : BaseComponentHolder<ApiComponent>() {
        override fun provide(): ApiComponent =
            DaggerApiComponent.builder()
                .apiModule(ApiModule())
                .contextDependencies(ContextComponent.get())
                .build()
    }
}