package com.pesiik.goodbadweather.inject.room

import android.content.Context
import androidx.room.Room
import com.pesiik.goodbadweather.room.AppDataBase
import dagger.Module
import dagger.Provides

@Module
class RoomModule {

    @Provides
    fun provideRoomDataBase(context: Context): AppDataBase =
        Room.databaseBuilder(context, AppDataBase::class.java, AppDataBase.DB_NAME).build()

}