package com.pesiik.goodbadweather.inject.base

abstract class BaseComponentHolder<C : Any> {

    private var component: C? = null

    private val syncObj = Any()

    fun get(): C {
        var componentVal = component
        if (componentVal == null) {
            synchronized(syncObj) {
                if (component == null) {
                    componentVal = provide()
                    component = componentVal!!
                }
            }
        }
        return componentVal!!
    }

    abstract fun provide(): C
}