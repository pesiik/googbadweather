package com.pesiik.goodbadweather.inject.manager

import com.google.gson.GsonBuilder
import com.pesiik.entities.city.City
import com.pesiik.entities.forecast.Forecast
import com.pesiik.goodbadweather.BuildConfig
import com.pesiik.goodbadweather.inject.retrofit.deserializers.ForecastableDeserializer
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Inject

class ApiManager @Inject constructor(
    client: OkHttpClient
) {

    private val gson = GsonBuilder()
        .registerTypeAdapter(City::class.java, ForecastableDeserializer())
        .registerTypeAdapter(Forecast::class.java, ForecastableDeserializer())
        .serializeNulls()
        .create()

    private val retrofit: Retrofit = Retrofit.Builder()
        .client(client)
        .addConverterFactory(GsonConverterFactory.create(gson))
        .baseUrl(BuildConfig.BASE_URL)
        .build()

    val apiService: ApiService = retrofit.create(ApiService::class.java)
}