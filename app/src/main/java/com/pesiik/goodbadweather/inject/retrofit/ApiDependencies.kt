package com.pesiik.goodbadweather.inject.retrofit

import com.pesiik.goodbadweather.inject.manager.ApiManager

interface ApiDependencies {
    fun apiManager(): ApiManager
}