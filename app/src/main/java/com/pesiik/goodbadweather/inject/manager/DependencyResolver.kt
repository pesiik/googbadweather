package com.pesiik.goodbadweather.inject.manager

import android.content.Context
import com.pesiik.goodbadweather.inject.context.ContextComponent

class DependencyResolver {
    fun init(context: Context) {
        ContextComponent.initProvider { ContextComponent.provide(context) }
    }
}