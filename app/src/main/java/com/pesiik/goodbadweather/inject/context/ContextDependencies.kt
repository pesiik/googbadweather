package com.pesiik.goodbadweather.inject.context

import android.content.Context

interface ContextDependencies {
    fun provideContext(): Context
}