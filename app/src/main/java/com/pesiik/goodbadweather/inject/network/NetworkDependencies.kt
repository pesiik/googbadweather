package com.pesiik.goodbadweather.inject.network

import com.pesiik.presentation.base.NetworkHandler

interface NetworkDependencies {
    fun provideNetworkHandler(): NetworkHandler
}