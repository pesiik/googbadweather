package com.pesiik.goodbadweather.inject.room

import com.pesiik.goodbadweather.inject.base.BaseComponentHolder
import com.pesiik.goodbadweather.inject.context.ContextComponent
import com.pesiik.goodbadweather.inject.context.ContextDependencies
import dagger.Component

@Component(
    modules = [RoomModule::class],
    dependencies = [ContextDependencies::class]
)
interface RoomComponent : RoomDependencies {
    companion object : BaseComponentHolder<RoomComponent>() {
        override fun provide(): RoomComponent =
            DaggerRoomComponent.builder()
                .contextDependencies(ContextComponent.get())
                .build()
    }
}