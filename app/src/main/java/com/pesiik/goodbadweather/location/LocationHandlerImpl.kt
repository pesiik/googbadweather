package com.pesiik.goodbadweather.location

import android.content.Context
import com.google.android.gms.location.*
import com.pesiik.entities.common.AppResult
import com.pesiik.entities.common.Location
import com.pesiik.interactor.location.LocationHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.suspendCancellableCoroutine
import javax.inject.Inject
import kotlin.coroutines.resume

class LocationHandlerImpl @Inject constructor(
    private val context: Context
) : LocationHandler {
    override suspend fun getMyLocation(): AppResult<Location> =
        suspendCancellableCoroutine { cont ->
            CoroutineScope(Dispatchers.Main).launch {
                val client = LocationServices.getFusedLocationProviderClient(context)
                client.requestLocationUpdates(LocationRequest(), object : LocationCallback() {
                    override fun onLocationAvailability(locationAvailability: LocationAvailability) {
                        super.onLocationAvailability(locationAvailability)
                        if (!locationAvailability.isLocationAvailable) {
                            cont.resume(
                                AppResult.Error(
                                    AppResult.Error.Reason.LOCATION_ERROR,
                                    "location is not available"
                                )
                            )
                        }

                    }

                    override fun onLocationResult(location: LocationResult) {
                        super.onLocationResult(location)
                        cont.resume(
                            AppResult.Success(
                                Location(
                                    lon = location.lastLocation.longitude,
                                    lat = location.lastLocation.latitude
                                )
                            )
                        )
                    }
                }, null)
            }
        }
}