package com.pesiik.goodbadweather

import android.app.Application
import com.pesiik.goodbadweather.inject.manager.DependencyResolver
import com.squareup.picasso.Picasso

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        DependencyResolver().init(applicationContext)
    }

}