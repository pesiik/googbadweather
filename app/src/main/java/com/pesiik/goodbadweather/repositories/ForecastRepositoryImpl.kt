package com.pesiik.goodbadweather.repositories

import com.pesiik.entities.common.AppResult
import com.pesiik.entities.forecast.Forecast
import com.pesiik.goodbadweather.inject.manager.ApiManager
import com.pesiik.goodbadweather.room.AppDataBase
import com.pesiik.interactor.extension.toCoroutineWithAppResult
import com.pesiik.interactor.repositories.ForecastRepository
import javax.inject.Inject

class ForecastRepositoryImpl @Inject constructor(
    private val apiManager: ApiManager,
    private val database: AppDataBase
) : ForecastRepository {

    override suspend fun getForecastsByCityIdFromRest(cityId: Int): AppResult<List<Forecast>> {
        val forecastsRoot = apiManager
            .apiService
            .getForecastsByCityId(cityId)
            .toCoroutineWithAppResult()
        return if (forecastsRoot is AppResult.Success) {
            AppResult.Success(forecastsRoot.value.list)
        } else {
            AppResult.Error(reason = (forecastsRoot as AppResult.Error).reason)
        }
    }
}