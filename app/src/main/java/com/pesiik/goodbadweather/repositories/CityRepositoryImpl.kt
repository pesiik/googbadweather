package com.pesiik.goodbadweather.repositories

import com.pesiik.entities.city.City
import com.pesiik.entities.common.AppResult
import com.pesiik.goodbadweather.extensions.toCitiesModels
import com.pesiik.goodbadweather.extensions.toEntities
import com.pesiik.goodbadweather.extensions.toModel
import com.pesiik.goodbadweather.inject.manager.ApiManager
import com.pesiik.goodbadweather.room.AppDataBase
import com.pesiik.goodbadweather.sharedprefs.SharedPreferencesManager
import com.pesiik.interactor.extension.toCoroutineWithAppResult
import com.pesiik.interactor.repositories.CityRepository
import javax.inject.Inject

class CityRepositoryImpl @Inject constructor(
    private val apiManager: ApiManager,
    private val database: AppDataBase,
    private val sharedPreferencesManager: SharedPreferencesManager
) : CityRepository {

    override suspend fun getNearestCities(lon: Double, lat: Double): AppResult<List<City>> {
        val cities = apiManager.apiService.getCitiesAroundGeolocation(
            lon = lon,
            lat = lat
        ).toCoroutineWithAppResult()
        return if (cities is AppResult.Success) {
            AppResult.Success(cities.value.list)
        } else {
            AppResult.Error((cities as AppResult.Error).reason)
        }
    }

    override suspend fun getCitiesFromDb(): AppResult<List<City>> {
        val cities = database.cityDao().getAllCities()
        return if (cities.isNotEmpty()) {
            AppResult.Success(cities.toCitiesModels())
        } else {
            AppResult.Error(reason = AppResult.Error.Reason.DB_ERROR)
        }
    }

    override suspend fun getCityDataById(id: Int): AppResult<City> {
        val city = database.cityDao().getCityById(id)
        return if (city != null) {
            AppResult.Success(city.toModel())
        } else {
            AppResult.Error(AppResult.Error.Reason.DB_ERROR)
        }
    }

    override suspend fun updateDefaultCity(city: City) {
        sharedPreferencesManager.preferences.edit().apply {
            putInt(SharedPreferencesManager.CITY_ID, city.id)
            apply()
        }
        insertCities(listOf(city))
    }

    override suspend fun getDefaultCity(): AppResult<City> {
        val defaultCityId = sharedPreferencesManager.defaultCityId
        return if (defaultCityId != SharedPreferencesManager.DEFAULT_CITY_ID) {
            getCityDataById(defaultCityId)
        } else {
            AppResult.Error(
                reason = AppResult.Error.Reason.NULL,
                message = "No default city",
                cause = IllegalStateException("Don't have default city")
            )
        }
    }

    override suspend fun getCityByGeolocation(lon: Double, lat: Double): AppResult<City> =
        apiManager.apiService.getCityByGeolocation(lon = lon, lat = lat).toCoroutineWithAppResult()

    override suspend fun insertCities(cities: List<City>) {
        database.cityDao().insertCities(cities.toEntities())
    }
}