package com.pesiik.interactor.interactors

import com.pesiik.entities.common.AppResult
import com.pesiik.entities.forecast.Forecast

interface ForecastInteractor {
    suspend fun getHourlyForecastByCityIdFromRest(cityId: Int): AppResult<List<Forecast>>
    suspend fun getDailyForecastByCityIdFromRest(cityId: Int): AppResult<List<Forecast>>
}