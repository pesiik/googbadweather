package com.pesiik.interactor.interactors

import com.pesiik.entities.city.City
import com.pesiik.entities.common.AppResult

interface NearestCitiesInteractor {
    suspend fun getNearestCities(): AppResult<List<City>>
    suspend fun getNearestCity(): AppResult<City>
}