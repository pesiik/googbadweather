package com.pesiik.interactor.interactors

import com.pesiik.entities.city.City
import com.pesiik.entities.common.AppResult


interface CityInteractor {
    suspend fun getCityById(id: Int): AppResult<City>
    suspend fun getDefaultCity(): AppResult<City>
    suspend fun updateDefaultCity(city: City)
    suspend fun isItDefaultCity(city: City): AppResult<Boolean>
    suspend fun getCitiesFromDb(): AppResult<List<City>>
}