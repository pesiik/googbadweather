package com.pesiik.interactor.repositories

import com.pesiik.entities.common.AppResult
import com.pesiik.entities.forecast.Forecast

interface ForecastRepository {
    suspend fun getForecastsByCityIdFromRest(cityId: Int): AppResult<List<Forecast>>
}