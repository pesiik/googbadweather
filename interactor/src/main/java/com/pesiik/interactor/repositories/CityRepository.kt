package com.pesiik.interactor.repositories

import com.pesiik.entities.city.City
import com.pesiik.entities.common.AppResult

interface CityRepository {
    suspend fun getNearestCities(lon: Double, lat: Double): AppResult<List<City>>
    suspend fun getCitiesFromDb(): AppResult<List<City>>
    suspend fun getCityDataById(id: Int): AppResult<City>
    suspend fun getCityByGeolocation(lon: Double, lat: Double): AppResult<City>
    suspend fun insertCities(cities: List<City>)
    suspend fun getDefaultCity(): AppResult<City>
    suspend fun updateDefaultCity(city: City)
}