package com.pesiik.interactor.location

import com.pesiik.entities.common.AppResult
import com.pesiik.entities.common.Location

interface LocationHandler {
    suspend fun getMyLocation(): AppResult<Location>
}