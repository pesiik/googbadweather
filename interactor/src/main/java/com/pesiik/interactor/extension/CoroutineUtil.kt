package com.pesiik.interactor.extension

import com.pesiik.entities.common.AppResult
import kotlinx.coroutines.suspendCancellableCoroutine
import retrofit2.Call
import retrofit2.Callback
import retrofit2.HttpException
import retrofit2.Response
import kotlin.coroutines.Continuation
import kotlin.coroutines.resume

suspend fun <A : Any> Call<A>.toCoroutineWithAppResult(): AppResult<A> =
    suspendCancellableCoroutine { continuation: Continuation<AppResult<A>> ->
        enqueue(object : Callback<A> {
            override fun onFailure(call: Call<A>, throwable: Throwable) {
                if (throwable is HttpException) {
                    when (throwable.code()) {
                        403 -> {
                            continuation.resume(
                                AppResult.Error(
                                    reason = AppResult.Error.Reason.CALL_PER_MINUTE,
                                    message = throwable.message(),
                                    cause = throwable
                                )
                            )
                        }
                        else -> {
                            continuation.resume(
                                AppResult.Error(
                                    reason = AppResult.Error.Reason.SERVER_ERROR,
                                    message = throwable.message(),
                                    cause = throwable
                                )
                            )
                        }
                    }

                } else {
                    continuation.resume(
                        AppResult.Error(
                            reason = AppResult.Error.Reason.UNKNOWN,
                            message = throwable.message ?: "No message",
                            cause = throwable
                        )
                    )
                }
            }

            override fun onResponse(call: Call<A>, response: Response<A>) {
                response.body()?.let { obj ->
                    continuation.resume(AppResult.Success(obj))
                } ?: continuation.resume(AppResult.Error(
                    reason = AppResult.Error.Reason.SERVER_ERROR,
                    message = response.errorBody()!!.string()
                ))
            }
        })
    }
