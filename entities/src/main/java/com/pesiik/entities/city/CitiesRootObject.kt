package com.pesiik.entities.city

data class CitiesRootObject(
    val message: String,
    val cod: String,
    val count: Int,
    val list: List<City>
)