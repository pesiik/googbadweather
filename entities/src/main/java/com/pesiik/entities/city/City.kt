package com.pesiik.entities.city

import com.pesiik.entities.common.Forecastable


data class City(
    val id: Int,
    val name: String,
    val lat: Double,
    val lon: Double,
    val country: Country?,
    override val icon: String?,
    override val temperature: Float?,
    override val pressure: Int?,
    override val humidity: Int?,
    override val minTemperature: Float?,
    override val maxTemperature: Float?,
    override val windSpeed: Int?,
    override val windDegrees: Int?,
    override val date: Long,
    override val clouds: Int?
) : Forecastable {

    enum class Country {
        RU,
        US
    }
}