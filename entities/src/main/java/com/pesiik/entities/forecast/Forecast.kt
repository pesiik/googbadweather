package com.pesiik.entities.forecast

import com.pesiik.entities.common.Forecastable

data class Forecast(
    override val icon: String?,
    override val temperature: Float?,
    override val pressure: Int?,
    override val humidity: Int?,
    override val minTemperature: Float?,
    override val maxTemperature: Float?,
    override val windSpeed: Int?,
    override val windDegrees: Int?,
    override val date: Long,
    override val clouds: Int?
) : Forecastable