package com.pesiik.entities.forecast

data class ForecastRootObject(
    val cod: String,
    val message: Float,
    val cnt: Int,
    val list: List<Forecast>
)