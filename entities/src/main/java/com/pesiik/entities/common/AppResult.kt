package com.pesiik.entities.common

sealed class AppResult<out T> {
    data class Success<out T>(val value: T) : AppResult<T>()
    data class Error(
        val reason: Reason = Reason.UNKNOWN,
        val message: String = "No error message",
        val cause: Throwable? = null
    ) : AppResult<Nothing>() {
        enum class Reason {
            NO_CONNECTION,
            CALL_PER_MINUTE,
            SERVER_ERROR,
            UNKNOWN,
            NULL,
            LOCATION_ERROR,
            NO_PERMISSIONS,
            DB_ERROR
        }

        constructor(cause: Throwable) : this(message = cause.message ?: "", cause = cause)

        override fun toString(): String {
            return "$reason $message"
        }
    }

    fun <R> mapNotNull(transform: (T) -> R?): AppResult<R> {
        return when (this) {
            is Success -> {
                val transformed = transform(value)
                if (transformed != null) {
                    Success(transformed)
                } else {
                    Error() // TODO specify error
                }
            }
            is Error -> this
        }
    }

    fun <R> map(transform: (T) -> R): AppResult<R> {
        return when (this) {
            is Success -> Success(transform(value))
            is Error -> this
        }
    }
}