package com.pesiik.entities.common

data class Location(
    val lat: Double,
    val lon: Double
)