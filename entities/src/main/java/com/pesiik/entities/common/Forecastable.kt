package com.pesiik.entities.common

interface Forecastable {
    val icon: String?
    val temperature: Float?
    val pressure: Int?
    val humidity: Int? //влажность
    val minTemperature: Float?
    val maxTemperature: Float?
    val windSpeed: Int?
    val windDegrees: Int?
    val date: Long
    val clouds: Int?

    enum class WindDirection {
        North,
        NorthEast,
        East,
        SouthEast,
        South,
        SouthWest,
        West,
        NorthWest;
    }

    fun getDirection(): WindDirection =
        when (windDegrees) {
            in 0..44 -> WindDirection.North
            in 45..90 -> WindDirection.NorthEast
            in 91..135 -> WindDirection.East
            in 136..180 -> WindDirection.SouthEast
            in 181..225 -> WindDirection.South
            in 226..270 -> WindDirection.SouthWest
            in 270..315 -> WindDirection.West
            in 315..359 -> WindDirection.NorthWest
            else -> throw IllegalStateException("No direction for these degrees")
        }
}