package com.pesiik.presentation.base

interface NetworkHandler {
    suspend fun isConnect(): Boolean
}