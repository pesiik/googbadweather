package com.pesiik.presentation.base

import android.os.Bundle
import com.pesiik.presentation.navigation.NavigationDestination

interface NavigationHandler {
    fun navigateTo(navigationDestination: NavigationDestination, bundle: Bundle? = null)
}