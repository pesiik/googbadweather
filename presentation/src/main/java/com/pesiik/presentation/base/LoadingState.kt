package com.pesiik.presentation.base

enum class LoadingState {
    LOADING,
    DONE
}