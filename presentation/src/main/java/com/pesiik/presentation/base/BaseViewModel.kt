package com.pesiik.presentation.base

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import com.pesiik.entities.common.AppResult

abstract class BaseViewModel : ViewModel() {

    var failure: MutableLiveData<AppResult.Error> = MutableLiveData()
    val loadingState = MutableLiveData<LoadingState>()

    private val loadingObserver = Observer<AppResult.Error> {
        loadingState.postValue(LoadingState.DONE)
    }

    init {
        failure.observeForever(loadingObserver)
    }

    override fun onCleared() {
        super.onCleared()
        failure.removeObserver(loadingObserver)
    }

    protected fun postUnknownError(throwable: Throwable) {
        failure.postValue(
            AppResult.Error(
                reason = AppResult.Error.Reason.UNKNOWN,
                message = throwable.message ?: "No message",
                cause = throwable
            )
        )
    }
}