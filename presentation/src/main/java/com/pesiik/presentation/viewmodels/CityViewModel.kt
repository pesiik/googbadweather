package com.pesiik.presentation.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.pesiik.entities.city.City
import com.pesiik.entities.common.AppResult
import com.pesiik.interactor.interactors.CityInteractor
import com.pesiik.interactor.interactors.NearestCitiesInteractor
import com.pesiik.presentation.base.BaseViewModel
import com.pesiik.presentation.base.LoadingState
import com.pesiik.presentation.base.NetworkHandler
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

class CityViewModel @Inject constructor(
    private val cityInteractor: CityInteractor,
    private val nearestCitiesInteractor: NearestCitiesInteractor,
    private val networkHandler: NetworkHandler
) : BaseViewModel() {

    val cityLiveData = MutableLiveData<City>()
    val isEmpty = MutableLiveData<Boolean>()

    fun updateCityById(id: Int) {
        viewModelScope.launch(Dispatchers.IO) {
            loadingState.postValue(LoadingState.LOADING)
            val result = cityInteractor.getCityById(id)
            if (result is AppResult.Success) {
                cityLiveData.postValue(result.value)
                isEmpty.postValue(false)
                loadingState.postValue(LoadingState.DONE)
            } else {
                getDefaultCity()
            }
        }
    }

    fun getDefaultCity() {
        viewModelScope.launch(Dispatchers.IO) {
            loadingState.postValue(LoadingState.LOADING)
            val defCity = cityInteractor.getDefaultCity()
            if (defCity is AppResult.Success) {
                loadingState.postValue(LoadingState.DONE)
                isEmpty.postValue(false)
                cityLiveData.postValue(defCity.value)
            } else {
                if (networkHandler.isConnect()) {
                    val nearestCity = nearestCitiesInteractor.getNearestCity()
                    if (nearestCity is AppResult.Success) {
                        loadingState.postValue(LoadingState.DONE)
                        cityInteractor.updateDefaultCity(nearestCity.value)
                        isEmpty.postValue(false)
                        cityLiveData.postValue(nearestCity.value)
                    } else {
                        loadingState.postValue(LoadingState.DONE)
                        isEmpty.postValue(true)
                        failure.postValue(nearestCity as AppResult.Error)
                    }
                } else {
                    failure.postValue(AppResult.Error(AppResult.Error.Reason.NO_CONNECTION))
                }
            }
        }
    }
}