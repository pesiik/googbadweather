package com.pesiik.presentation.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.pesiik.entities.common.AppResult
import com.pesiik.entities.forecast.Forecast
import com.pesiik.interactor.interactors.ForecastInteractor
import com.pesiik.presentation.base.BaseViewModel
import com.pesiik.presentation.base.LoadingState
import com.pesiik.presentation.base.NetworkHandler
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

class ForecastViewModel @Inject constructor(
    private val forecastInteractor: ForecastInteractor,
    private val networkHandler: NetworkHandler
) : BaseViewModel() {

    val forecasts = MutableLiveData<List<Forecast>>()

    fun updateHourlyForecastsFromApi(cityId: Int) =
        viewModelScope.launch(Dispatchers.IO) {
            if (networkHandler.isConnect()) {
                updateHourlyForecastsFromRest(cityId)
            } else {
                failure.postValue(AppResult.Error(AppResult.Error.Reason.NO_CONNECTION))
            }
        }

    fun updateDailyForecastsFromApi(cityId: Int) =
        viewModelScope.launch(Dispatchers.IO) {
            if (networkHandler.isConnect()) {
                updateDailyForecastsFromRest(cityId)
            } else {
                failure.postValue(AppResult.Error(AppResult.Error.Reason.NO_CONNECTION))
            }
        }

    private suspend fun updateHourlyForecastsFromRest(cityId: Int) {
        val forecasts = forecastInteractor.getHourlyForecastByCityIdFromRest(cityId)
        if (forecasts is AppResult.Success) {
            this@ForecastViewModel.forecasts.postValue(forecasts.value)
            loadingState.postValue(LoadingState.DONE)
        } else {
            failure.postValue(forecasts as AppResult.Error)
        }
    }

    private suspend fun updateDailyForecastsFromRest(cityId: Int) {
        val forecasts = forecastInteractor.getDailyForecastByCityIdFromRest(cityId)
        if (forecasts is AppResult.Success) {
            this@ForecastViewModel.forecasts.postValue(forecasts.value)
            loadingState.postValue(LoadingState.DONE)
        } else {
            failure.postValue(forecasts as AppResult.Error)
        }
    }
}
