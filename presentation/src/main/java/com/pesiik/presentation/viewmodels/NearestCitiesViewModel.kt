package com.pesiik.presentation.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.pesiik.entities.city.City
import com.pesiik.entities.common.AppResult
import com.pesiik.interactor.interactors.CityInteractor
import com.pesiik.interactor.interactors.NearestCitiesInteractor
import com.pesiik.presentation.base.BaseViewModel
import com.pesiik.presentation.base.LoadingState
import com.pesiik.presentation.base.NetworkHandler
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

class NearestCitiesViewModel @Inject constructor(
    private val nearestCitiesInteractor: NearestCitiesInteractor,
    private val cityInteractor: CityInteractor,
    private val networkHandler: NetworkHandler
) : BaseViewModel() {

    val cities = MutableLiveData<List<City>>()
    val nearestCity = MutableLiveData<City>()
    val selectedCity = MutableLiveData<City>()

    var isDefaultCityUpdated = false
    var isMyCityButtonClick = false

    fun updateNearestCities() {
        viewModelScope.launch(Dispatchers.IO) {
            loadingState.postValue(LoadingState.LOADING)
            val isConnect = networkHandler.isConnect()
            if (isConnect) {
                val cities = nearestCitiesInteractor.getNearestCities()
                if (cities is AppResult.Success) {
                    loadingState.postValue(LoadingState.DONE)
                    this@NearestCitiesViewModel.cities.postValue(cities.value)
                } else {
                    this@NearestCitiesViewModel.loadingState.postValue(LoadingState.DONE)
                    failure.postValue(cities as AppResult.Error)
                }
            } else {
                val cities = cityInteractor.getCitiesFromDb()
                if (cities is AppResult.Success) {
                    this@NearestCitiesViewModel.loadingState.postValue(LoadingState.DONE)
                    this@NearestCitiesViewModel.cities.postValue(cities.value)
                } else {
                    this@NearestCitiesViewModel.loadingState.postValue(LoadingState.DONE)
                    failure.postValue(AppResult.Error(AppResult.Error.Reason.NO_CONNECTION))
                }
            }
        }
    }

    fun updateNearestCity() {
        viewModelScope.launch(Dispatchers.IO) {
            if (networkHandler.isConnect()) {
                loadingState.postValue((LoadingState.LOADING))
                val nearestCity = nearestCitiesInteractor.getNearestCity()
                if (nearestCity is AppResult.Success) {
                    loadingState.postValue((LoadingState.DONE))
                    isCityDefault(nearestCity.value)
                } else {
                    this@NearestCitiesViewModel.loadingState.postValue(LoadingState.DONE)
                    failure.postValue(nearestCity as AppResult.Error)
                }
            } else {
                val nearestCity = cityInteractor.getDefaultCity()
                if (nearestCity is AppResult.Success) {
                    loadingState.postValue((LoadingState.DONE))
                    this@NearestCitiesViewModel.selectedCity.postValue(nearestCity.value)
                } else {
                    loadingState.postValue(LoadingState.DONE)
                    failure.postValue(AppResult.Error(AppResult.Error.Reason.DB_ERROR))
                }
            }
        }
    }

    fun updateDefaultCity(city: City) {
        viewModelScope.launch(Dispatchers.IO) {
            cityInteractor.updateDefaultCity(city)
        }
    }

    fun selectCity() {
        nearestCity.value?.let {
            selectedCity.postValue(it)
        }
    }

    private fun isCityDefault(city: City) {
        viewModelScope.launch(Dispatchers.IO) {
            val isItDefaultCity = cityInteractor.isItDefaultCity(city)
            isDefaultCityUpdated = !(isItDefaultCity is AppResult.Success && isItDefaultCity.value)
            nearestCity.postValue(city)
            if (isMyCityButtonClick) {
                selectedCity.postValue(city)
            }
        }
    }
}
