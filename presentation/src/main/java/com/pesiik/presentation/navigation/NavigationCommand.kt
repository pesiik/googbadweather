package com.pesiik.presentation.navigation

import com.pesiik.presentation.base.SingleLiveEvent

class NavigationCommand : SingleLiveEvent<NavigationDestination>()