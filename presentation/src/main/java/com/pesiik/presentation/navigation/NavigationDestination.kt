package com.pesiik.presentation.navigation

sealed class NavigationDestination {

    object Back : NavigationDestination()

    object FromListToMyCity : NavigationDestination()

    object FromListToOtherCity : NavigationDestination()

    object FromListToConfirm : NavigationDestination()
}